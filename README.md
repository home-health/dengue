# Dengue

Dengue is a Docker image based on Alpine Linux that contains NGINX Web Server that acts as a proxy for
[Ebola](https://gitlab.com/home-health/ebola).

## Authentication

Authenticate to the GitLab Container Registry before use this image.

```bash
docker login registry.gitlab.com -u <username> -p <token>
```

## Usage

Preferably, extend this image in a production environment.

```dockerfile
FROM registry.gitlab.com/home-health/dengue
COPY public public/
```

When running, this image listens for requests on `127.0.0.1:80`.

## Contributing

See the [Contribution Guide](CONTRIBUTING.md).

## License

See the [End-User License Agreement (EULA)](LICENSE.md).
