# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this project adheres to
[Semantic Versioning](https://semver.org).

## [Unreleased]

## [1.1.21] - 2020-07-13

### Changed
- Docker image based on Alpine Linux and NGINX 1.19.1.
- Specification of Ebola 1.3.8 as a Continuous Integration linkable image.

## [1.1.20] - 2020-07-04

### Changed
- Specification of Ebola 1.3.7 as a Continuous Integration linkable image.
- Specification of Docker 19.03.12 as a Continuous Integration image.
- Specification of Docker DinD 19.03.12 as a Continuous Integration service.

## [1.1.19] - 2020-06-14

### Changed
- Specification of Ebola 1.3.6 as a Continuous Integration linkable image.

## [1.1.18] - 2020-06-02

### Changed
- Docker image based on Alpine Linux and NGINX 1.19.0.
- Specification of Docker 19.03.11 as a Continuous Integration image.
- Specification of Docker DinD 19.03.11 as a Continuous Integration service.
- Specification of Ebola 1.3.5 as a Continuous Integration linkable image.

## [1.1.17] - 2020-05-27

### Changed
- Specification of Docker 19.03.9 as a Continuous Integration image.
- Specification of Docker DinD 19.03.9 as a Continuous Integration service.
- Specification of Ebola 1.3.4 as a Continuous Integration linkable image.

## [1.1.16] - 2020-05-03

### Changed
- Specification of Ebola 1.3.3 as a Continuous Integration linkable image.
- Demonstrate image extension using relative path copy.

## [1.1.15] - 2020-04-27

### Changed
- Docker image based on Alpine Linux and NGINX 1.18.0.

## [1.1.14] - 2020-04-17

### Changed
- Docker image based on Alpine Linux and NGINX 1.17.10.
- Specification of Ebola 1.3.2 as a Continuous Integration linkable image.

## [1.1.13] - 2020-03-30

### Changed
- Replace `REMOTE_ADDR` with `HTTP_X_REAL_IP`.

## [1.1.12] - 2020-03-29

### Changed
- Correct indentation of NGINX configuration file.

## [1.1.11] - 2020-03-28

### Changed
- Compress all types of response.

## [1.1.10] - 2020-03-27

### Changed
- Specification of Docker 19.03.8 as a Continuous Integration image.
- Specification of Docker DinD 19.03.8 as a Continuous Integration service.
- Specification of Ebola 1.3.1 as a Continuous Integration linkable image.

## [1.1.9] - 2020-03-13

### Changed
- Specification of Ebola 1.3.0 as a Continuous Integration linkable image.
- Reorder commands in test script alphabetically.

## [1.1.8] - 2020-03-11

### Changed
- Specification of Docker 19.03.7 as a Continuous Integration image.
- Specification of Docker DinD 19.03.7 as a Continuous Integration service.
- Specification of Ebola 1.2.1 as a Continuous Integration linkable image.
- Docker image based on Alpine Linux and NGINX 1.17.9.
- Use apostrophe character for strings.

## [1.1.7] - 2020-02-25

### Changed
- Specification of Ebola 1.2.0 as a Continuous Integration linkable image.

## [1.1.6] - 2020-02-23

### Changed
- Reorder attributes in Continuous Integration configuration file alphabetically.
- Specification of Ebola 1.1.5 as a Continuous Integration linkable image.

## [1.1.5] - 2020-02-22

### Changed
- Project description.
- Specification of Ebola 1.1.4 as a Continuous Integration linkable image.

## [1.1.4] - 2020-02-21

### Changed
- Specification of Ebola 1.1.3 as a Continuous Integration linkable image.
- Specification of Docker 19.03.5 as a Continuous Integration image.

## [1.1.3] - 2020-02-21

### Changed
- Specification of Docker DinD 19.03.5 as a Continuous Integration service.
- Limit 120 characters per line.
- Indent lines with 4 spaces.

## [1.1.2] - 2020-02-12

### Fixed
- Continuous Integration file reusability.
- Continuous Integration file verbosity.
- Continuous Integration file indenting.

### Removed
- Unnecessary script in Continuous Integration.

## [1.1.1] - 2020-02-11

### Changed
- Use parent folder of NGINX root directory as workdir.

## [1.1.0] - 2020-02-10

### Changed
- Automatically manage NGINX root directory permissions.

## [1.0.1] - 2020-02-10

### Fixed
- Default NGINX root directory.

## [1.0.0] - 2020-02-10

### Added
- Project readme.
- Continuous Integration.
- NGINX configuration file.
- Remove unnecessary files from Docker build context.
- Test script.
- Docker image based on Alpine Linux and NGINX 1.17.8.
- Project license.
- Contributing guide.
- Record of all notable changes made to this project.
